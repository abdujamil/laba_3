using NUnit.Framework;
using System;
using Biblio_TRPO;

namespace NUnitTest_laba_3
{
    public class Tests
    {
        [SetUp]
        public void Tst1()
        {
        }

        [Test]
        public void Tst2()
        {
            //Biblio_TRPO.Math_1 math = new Biblio_TRPO.Math_1();
            const double i = 656.59;
            double s = Math_1.Get_1(15, 4);
            Assert.Equals(i, s);
        }

        [Test]
       public void Testing()
        {
            Assert.Throws<ArgumentException>(() => Math_1.Get_1(-15, -4));
        }
    }
}