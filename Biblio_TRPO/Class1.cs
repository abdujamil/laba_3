﻿using System;

namespace Biblio_TRPO
{
    public class Math_1
    {
        public double Or;
        public double ir;

        public static double Get_1(double Or, double ir)
        {
            if(Or < ir)
            {
                throw new ArgumentException("Внешний радиус больше внутреннего!");   
            }
            double r = Math.PI * (Math.Pow(Or, 2) - Math.Pow(ir, 2));
            return Math.Round(r, 2);
        }

    }
}
